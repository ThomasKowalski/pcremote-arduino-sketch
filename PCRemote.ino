//Importation des librairies
#include <IRremote.h>
#include <LiquidCrystal.h>

//Définition des différentes variables :
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); 
int IRpin = A0; //Pin des données de la diode infrarouge
int led = 5; //Pin inutile pour le moment
int incomingByte = 0; //Servira à récupérer les données sur le port série
IRrecv irrecv(IRpin); 
decode_results results; //Résultat
String chaine = ""; //Chaine reçue 

void setup()
{
  Serial.begin(9600);
  //pinMode(led, OUTPUT);
  pinMode(IRpin, INPUT);
  lcd.begin(16, 2);
  lcd.print("Systeme demarre");
  lcd.setCursor(0, 1);
  lcd.print(" Att. connexion");
  irrecv.enableIRIn();
}

void loop() 
{
  if (irrecv.decode(&results)) //Partie 1 : si on a reçu un signal sur la diode
  {
    int resultat = 0;
    int tempResultat = results.value;
    if (tempResultat >= 2048) { //Permet de filtrer les résultats. Au moins un petit peu.
      resultat = tempResultat - 2048;
    } 
    else {
      resultat = tempResultat; 
    }
    Serial.println(resultat); //On envoie le signal sur le port série 
    irrecv.resume(); //Et on réaccepte les signaux
  }
  if (Serial.available() > 0) { //Partie 2 : si on a reçu quelque chose sur le port série
    incomingByte = Serial.read(); //Numéro ASCII reçu
    //Serial.println(String(incomingByte));
    char fromSerial = incomingByte;
    if (fromSerial != 13) { //Si jamais ce n'est pas un CR
      if (fromSerial == 49) //Si c'est un 1 (code 49) ça veut dire qu'on affiche un nouveau truc
        lcd.clear();
      if(fromSerial > 31 && fromSerial < 126) //On filtre les caractères pour éviter les non imprimables
        chaine = chaine + fromSerial; //On ajoute à la chaîne
    } else { //Si jamais c'est un CR
      String stringToDisplay = chaine.substring(1, chaine.length()); //On enlève le premier caractère (numéro de ligne)
      if(Contains(stringToDisplay, "ping"))
        Serial.println("pong");
      if (chaine.substring(0, 1) == "1") //Si le premier caractère est un 1
        lcd.setCursor(0, 0); //on écrit sur la première ligne
      else if (chaine.substring(0, 1) == "2") //sinon 
        lcd.setCursor(0, 1); //on écrit sur la deuxième ligne
      if (chaine.substring(0, 1) != "3") { //si c'est une information à afficher et pas une communication système
        lcd.print(stringToDisplay); //on écrit la chaîne
        if (Contains(stringToDisplay, "Connecte")) //Si jamais l'ordinateur vient de se connecter, on lui envoie "ok" pour confirmer qu'il s'est connecté à un périphérique COM compatible PCRemote.
  	  Serial.println("ok");
      }
      chaine = ""; //on la réinitialise
      stringToDisplay = "";
    }
  }
}

bool Contains(String s, String search) { //Fonction qui permet de savoir si la chaine s contient la chaine search
    int max = s.length() - search.length();
    for (int i = 0; i <= max; i++) {
        if (s.substring(i) == search) return true; // or i
    }
    return false; //or -1
}



