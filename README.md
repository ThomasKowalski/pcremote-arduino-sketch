# README #

Ce sketch a été créé pour fonctionner avec le serveur PCRemote.
Vous pouvez trouver plus d'informations à son propos [ici](http://thomaskowalski.net/pcremote)

### Comment télécharger PCRemote ? ###

Téléchargez le sketch en vous rendant dans la séction *Downloads* sur la droite, et cliquez sur *Download all repository*.

### Comment installer PCRemote ? ###

* Envoyez le sketch sur votre Arduino (configuré comme expliqué à [cette adresse](http://thomaskowalski.net/pcremote)).
* Téléchargez [le serveur](https://bitbucket.org/ThomasKowalski/pcremote-pc-server).
* Apprenez à configurer ce dernier 

### Dépendances externes ###

Vous aurez besoin de la librairie IRemote disponible [ici](https://github.com/shirriff/Arduino-IRremote).

### Besoin d'aide ? ###

N'hésitez pas à [me contacter](http://thomaskowalski.net/contact) et à allez voir [l'article officiel](http://thomaskowalski.net/pcremote) pour plus d'informations.